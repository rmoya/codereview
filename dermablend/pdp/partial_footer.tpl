{{!-- 
you can debug any object with writing {{debug}} on this template;
use {{debug}} or {{debug someValue}}
the result will be on browser's console
--}}
<div class="olapic-slider-footer">
	<div class="olapic-footer-buttons">
		{{>link_all_photos}}
		{{>link_uploader}}
	</div>
        <div class="olapic-slider-copy"><a href="http://olapic.com" target="_blank" title="Powered by Olapic">[i]</a></div>
</div>