<link rel="stylesheet" type="text/css" href="//olapic-data.s3.amazonaws.com/dermablend/fonts/fonts.css" />
<link rel="stylesheet" type="text/css" href="//olapic-data.s3.amazonaws.com/dermablend/font-icons/font-icons.css" />
<style>

/*ICONS*/

[class^="slider-olapic-icon-"]:before, 
[class*=" slider-olapic-icon-"]:before {
  font-family: "fontello";
}

[class^="slider-icon-"]:before,
[class*=" slider-icon-"]:before {
  font-family: "slider-icons";
}

[class^="slider-icon-"]:before,
[class*=" slider-icon-"]:before,
[class^="slider-olapic-icon-"]:before, 
[class*=" slider-olapic-icon-"]:before {
  font-style: normal;
  font-weight: normal;
  speak: none;
  display: inline-block;
  text-decoration: inherit;
  width: 1em;
  margin-right: .2em;
  text-align: center;
  /* opacity: .8; */
  /* For safety - reset parent styles, that can break glyph codes*/
  font-variant: normal;
  text-transform: none;
  /* fix buttons height, for twitter bootstrap */
  line-height: 1em;
  /* Animation center compensation - margins should be symmetric */
  /* remove if not needed */
  margin-left: .2em;
  /* you can be more comfortable with increased icons size */
  /* font-size: 120%; */
  /* Uncomment for 3D effect */
  /* text-shadow: 1px 1px 1px rgba(127, 127, 127, 0.3); */ }


.slider-olapic-icon-instagram-filled:before {
    content: '\e900'; 
}

.slider-icon-twitter:before {
  content: '\e800'; }

/* '' */
.slider-icon-attention:before {
  content: '\e801'; }

/* '' */
.slider-icon-vine:before {
  content: '\e802'; }

/* '' */
.slider-icon-desktop:before {
  content: '\e803'; }

/* '' */
.slider-icon-youtube-play:before {
  content: '\e804'; }

/* '' */
.slider-icon-pinterest:before {
  content: '\e805'; }

/* '' */
.slider-icon-attention-1:before {
  content: '\e806'; }

/* '' */
.slider-icon-instagram:before {
  content: '\e807'; }

/* '' */
.slider-icon-uploadicon:before {
  content: '\e808'; }

/* '' */
.slider-icon-tumblr:before {
  content: '\e809'; }

/* '' */
.slider-icon-instagram:before {
  content: '\e80a'; }

/* '' */
.slider-icon-cancel:before {
  content: '\e80b'; }

/* '' */
.slider-icon-close:before {
  content: '\e80c'; }

/* '' */
.slider-icon-facebook:before {
  content: '\e80d'; }

/* '' */
.slider-icon-spin2:before {
  content: '\e80e'; }

/* '' */
.slider-icon-ok:before {
  content: '\e80f'; }

/* '' */
.slider-icon-flickr:before {
  content: '\e813'; }

/* '' */
.slider-icon-olapic-logo:before {
  content: '\e814'; }

/* '' */
.slider-icon-youtube:before {
  content: '\e816'; }

/* '' */
.slider-icon-gplus:before {
  content: '\e817'; }

/* '' */
.slider-icon-down-open-big:before {
  content: '\e818'; }

/* '' */
.slider-icon-left-open-big:before {
  content: '\e819'; }

/* '' */
.slider-icon-right-open-big:before {
  content: '\e81a'; }

/* '' */
.slider-icon-up-open-big:before {
  content: '\e81b'; }

/* Widget Wrapper */

.olapic .olapic-slider-widget {
    width: 100%;
    max-width: 1080px;
    margin: 0 auto;
    overflow: hidden;
    font-family: akzidenz-grotesk_bqregular, AkzidenzGrotesk-Roman, Helvetica, sans-serif;
    font-size: 16px;
    color: #000;
    background: #fff;
}

/* end widget wrapper */

/* Carousel Animation */

.olapic-carousel-list-animation {
  -webkit-transition: all 0.3s ease-in-out;
  -moz-transition: all 0.3s ease-in-out;
  -o-transition: all 0.3s ease-in-out;
  transition: all 0.3s ease-in-out;
}

/* end carousel animation */

/* Header */
.olapic .olapic-slider-header {
    overflow: hidden;
    margin: 0 auto;
}
.olapic .header-desktop-version {
    width: 91%;
}

.olapic .header-mobile-version {
    display: none;
}

/* - Header Columns - */
.olapic .olapic-slider-header h2 {
    margin: 28px 0 0;
}

.olapic .olapic-slider-header h2 span {
    display: inline-block;
    width: 12%;
    height: 0;
    padding-bottom: 7.5%;
    margin: 0 8px;
    background-image: url(//olapic-data.s3.amazonaws.com/dermablend/images/swatches.png);
    background-size: 82%;
    background-repeat: no-repeat;
    background-position: center;
}

.olapic .olapic-slider-header h3 {
    margin: 0;
    padding-top: 5px;
    font-family: AkzidenzGrotesk-Light, akzidenz-grotesk_bq_lightRg, Helvetica, Arial, sans-serif;
}

.olapic .olapic-slider-header h3 span {
    font-family:'AkzidenzGrotesk-Roman', sans-serif;
}

.olapic .header-desktop-version h3 {
    font-family: AkzidenzGrotesk-Light, akzidenz-grotesk_bq_lightRg, Helvetica, Arial, sans-serif;
    font-size: 1.3em;
    letter-spacing: .02em;
}

.olapic .header-desktop-version h3 span {
    font-size: .9em;
    color: #f9b387;
}

.header-mobile-version .olapic-slider-cta {
    width: 50%;
}

.olapic-slider-cta {
    float: left;
}

.olapic-slider-sharing {
    float: right;
}

.olapic-slider-sharing i {
    display: inline-block;
    width: 28px;
    height: 28px;
    margin-left: 5px;
    text-align: center;
    line-height: 28px;
    -webkit-border-radius: 50%;
    border-radius: 50%;
    color: #fff;
    background: #fbb585;
}

.olapic-slider-sharing i::before {
    margin: 0;
}


/* - end header columns - */

/* end header */

/* Slider */

.olapic .olapic-slider-body {
    position: relative;
}

.olapic .olapic-slider-wrapper {
	position: relative;
	overflow: hidden;
	width: 92%;
	margin: 0 auto;
}

.olapic .olapic-carousel {
	position: relative;
	padding: 0;
	margin: 0;
	list-style: none;
}

/* - Carousel List Item - */

.olapic .olapic-carousel li {
	position: relative;
	float: left;
	height: 235px;
	width: 235px;
	overflow: hidden;
	margin: 0 10px 0 4px;
	background-repeat: no-repeat;
	background-position: center center;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
}

.olapic .olapic-carousel li a {
	display: block;
	height: 100%;
	width: 100%;
}

.olapic .olapic-carousel li a:hover {
	height: 100%;
	width: 100%;
}

.olapic-type-video{
	position: absolute;
	height: 100%;
	width: 100%;
}
.olapic-type-video i{
	position: absolute;
	top: 50%;
	left: 50%;
	z-index: 150;
	width: 65px;
	height: 68px;
	margin: -34px 0 0 -32px;
	background: url("//photorankstatics-a.akamaihd.net/static/images/Viewer2/olapic/video-controls.png") no-repeat -38px -30px;
	cursor: pointer;
}

/* - end carousel list item - */

.olapic .olapic-carousel-list-container {
	position: relative;
	width: 999999%;
	left: 0px;
	margin: 0 auto;
	padding-top: 12px;
	top: 0px;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}


/* - Partial carousel container - */

.olapic-carousel-partial-container {
    display: none;
    overflow: hidden;
}

/* -

– Partial Carousel Styles –
How to display partial carousel content for unfilled widgets (like a call-to-action banner). 

The following steps assume we are using a 3 image carousel:

1) Show the ".olapic-carousel-partial-container" div when there are a certain number of photos available. 
In this case, the partial-container will show only when there are 0, 1, or 2 photos in the carousel. 
(olapic_items_n is a utility class, which is appended to the carousel when 'n' number of photos are
loaded into it):
.olapic_items_0 .olapic-carousel-partial-container, 
.olapic_items_1 .olapic-carousel-partial-container,
.olapic_items_2 .olapic-carousel-partial-container
{ display: block }

2) set the ".olapic-carousel-list-container" div width to 100% for the 0, 1, 2 images
.olapic_items_0 .olapic-carousel-list-container,
.olapic_items_1 .olapic-carousel-list-container,
.olapic_items_2 .olapic-carousel-list-container
{ width: 100%; }

3) Go to @media (max-width: 480px) in your responsive styles,
and optionally hide the carousel images and only show the ".olapic-carousel-partial-container". Responsive styles section is at the bottom of this stylesheet

- */

/* - End of Partial carousel container - */


/* - Carousel Pagination - */

.olapic .olapic-pagination {
	bottom: 20px;
	height: 10px;
	list-style: none;
	position: absolute;
	right: 0;
}

.olapic .olapic-pagination ul {
	list-style: none;
}

.olapic .olapic-pagination li {
	float: left;
	height: 10px;
	width: 10px;
	margin-left: 6px;
}

.olapic .olapic-pagination li a {
	display: block;
	height: 10px;
	width: 10px;
}

.olapic .olapic-pagination li a {
	background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo1MTkzODVFNThBN0NFMzExQURCNERBNkVBN0NERTFFQyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpDMUU0NzdENDc3RTAxMUUzQUJGOUU0NjU0MzM4RDYzMyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpDMUU0NzdEMzc3RTAxMUUzQUJGOUU0NjU0MzM4RDYzMyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1LjEgV2luZG93cyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjM5NjU5RTdDMUM3RUUzMTFBNDVGRjY5N0U3RUM1MjVGIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjUxOTM4NUU1OEE3Q0UzMTFBREI0REE2RUE3Q0RFMUVDIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Gol56QAAAHpJREFUeNpiZEAAWSBWAmI+KP8TEN8D4scgDiNUUA+I5Rmwg0dAfJEZSEgBsSYDbsAPxF+ZoaZxMuAH7ExIbsIH+EEK/xOh8D9I4XsiFL4HKbxLhMI7IM98A+I/QCyGQ9FVIH7KiCQgBMSKQCwA5X8A4vtA/A7EAQgwACO9Ew7rkoMYAAAAAElFTkSuQmCC);
}

.olapic .olapic-pagination li.active a {
	background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo1MTkzODVFNThBN0NFMzExQURCNERBNkVBN0NERTFFQyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpEMjdBQzYwNTc3RTAxMUUzQUJGOUU0NjU0MzM4RDYzMyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpEMjdBQzYwNDc3RTAxMUUzQUJGOUU0NjU0MzM4RDYzMyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M1LjEgV2luZG93cyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjM5NjU5RTdDMUM3RUUzMTFBNDVGRjY5N0U3RUM1MjVGIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjUxOTM4NUU1OEE3Q0UzMTFBREI0REE2RUE3Q0RFMUVDIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+YMu2KQAAAINJREFUeNpi/DWjkAEKEoEYxNGF8i8DcT8QLwDi/0xQwZlAPA9JEQOUDRKbBeKwAHE4EKcx4AYpQLwPZGIOA2GQA1JoQIRCQ5DC30Qo/ANSeIIIhcdBCntA3sejCCTXBVK4D4iLcCgGiZUA8V4WqMAEID4LxLlAbAoVOw0VPwbiAAQYADsAGxVFog8oAAAAAElFTkSuQmCC);
}

/* - end carousel pagination - */

/* - Carousel Toggles - */

.olapic .olapic-nav{
	position: relative;
	height: 100%;
	width: 100%;
}

.olapic .olapic-nav-button {
    position: absolute;
    top: 15%;
    display: block;
	height: 100%;
	width: 4%;
    font-size: 2.5em;
}

.olapic .olapic-nav-button, 
.olapic a:hover, 
.olapic a:focus {
    text-decoration: none;
    color: #000;
}

.olapic .olapic-nav-button::before {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
    height: 0;
}

.olapic .olapic-nav-prev {
    left: 0;
}

.olapic .olapic-nav-next {
    right: 0;
}

.olapic_slide_vertical .olapic-nav-prev {
	background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAARCAYAAAAhUad0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTNui8sowAAACuSURBVDiNtc/BCQJBDIXhH1ywAxFZK7EEPYmlWNSKDSmeLUIXjJcdGIYZN4kzD3J6JB8BX1bAGVhbF0XEBW6AGyDAA+hbozEYxgRb0Rxohi1oDnx6YC2aA6/AEhiAjwXWoCWwm/pFAd560TkwxAT/QrWgGS6hVjCGLxo4B94doBpOP/0XVMExWgtUwS3AObgH2AOvymAJHoFDKI/AuzKYwiNwSstdAzCkm+4jInwBx8Cbei+J6sYAAAAASUVORK5CYII=);
}

.olapic_slide_vertical .olapic-nav-next {
	background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAARCAYAAAAhUad0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTNui8sowAAAC+SURBVDiNtdI9TkJBEADgjwiJJzCE6E28gbGx8CQcCgqug4U9d9AYxsL35IWwjx1YJplm5+crZvmLCZ4xdZuYdftFxD/4jh+scXcDcINvvPWPrx0Y2DeGezC6/MILPOFzUGgFH4OBDyz6htZwCZz3N20NF0EOH6klPAqW0GvgU+B2CI6hJXg1AleB59AMXA3WoDVwCqxF4bEA32fBDHoKDuyyYBYtwSnwErQEV4OXokN4nwWvQXXQEg/ZwYjwC5s+oW7hcDuyAAAAAElFTkSuQmCC);
	margin-top: -5px;
}

.olapic-slider-noArrows {
	visibility: hidden;
}

/* - end carousel toggles - */

/* Footer */

.olapic .olapic-slider-footer {
	width: 91%;
	min-height: 60px;
	margin: 0 auto;
}

.olapic .olapic-footer-buttons {
	padding-top: 9px;
    text-align: right;
}

.olapic .olapic-footer-button {
	display: inline-block;
}

.olapic .olapic-footer-button a {
	display: inline-block;
	font-size: .85em;
	text-decoration: none;
	text-align: center;
	color: #000;
}

.olapic-upload a::before {
    margin: 0 7px 0 2px;
    content: "|";
}

.olapic .olapic-slider-copy a {
	float: right;
	margin: 5px 0 0;
    font-size: 11px;
    color: #CCC;
    letter-spacing: 1px;
}

/* end carousel footer */

/* Vertical Slider Styles */

.olapic_slide_vertical {
	width: 250px;
}

.olapic_slide_vertical .olapic-slider-wrapper, 
.olapic_slide_vertical .olapic-slider-body {
	height: auto;
}

.olapic_slide_vertical .olapic-slider-wrapper {
    height: 471px;
}

.olapic_slide_vertical .olapic-carousel-list-container {
    top: 0px;
	height: 462px;
	padding-top: 0px;
	width: 100%;
}

.olapic_slide_vertical .olapic-carousel li {
	float: none;
	display: inline-block;
    margin: 0px auto 5px 39px
}

.olapic_slide_vertical .olapic-slider-footer { 
	margin-top: 15px;
}

.olapic_slide_vertical .olapic-slider-wrapper,
.olapic_slide_vertical .olapic-nav-button {
	float: none;
}

.olapic_slide_vertical .olapic-nav-button {
	height: 29px;
	margin: 0 auto;
}

/* end vertical slider styles */

/* Your Custom Styles Here */



/* end your custom styles here */

/* Responsive */

/* - Large Desktop - */

@media (min-width: 1200px) {

}

/* - end large desktop - */

/* - Landscape Tablet to Small Desktop - */

@media (min-width: 768px) and (max-width: 979px) {

}

/* - end landscape tablet to small desktop - */

/* - Landscape Phone To Portrait Tablet - */

@media (max-width: 768px) {
    
    .olapic .olapic-slider-body {
        padding-top: 17px;
        width: 78.5%;
        max-width: 588px;
        margin: auto;
        font-size: 9px;
        background-image: url(//olapic-data.s3.amazonaws.com/dermablend/images/swatches.png);
        background-size: 11%;
        background-position: 1% 2%;
        background-repeat: no-repeat;
    }
    
    .olapic .hp-block-title {
        display: none;
    }
    
    .olapic .header-desktop-version {
        width: 74%;
        max-width: 432px;
    }
    
    .olapic .header-desktop-version h3 {
        letter-spacing: .04em;
        font-family: 'akzidenz-grotesk_bqbold',  AkzidenzGrotesk-Bold, Helvetica, sans-serif;;
        color: #f9b387;
    }
    
    .olapic .header-desktop-version h3 span {
        letter-spacing: .05em;
        color: #000;
    }
    
    .olapic-slider-body .olapic-slider-sharing {
        padding: 2px 0;
    }
    
    .olapic-slider-sharing i {
        width: 16px;
        height: 16px;
        margin-left: 4px;
        font-size: 1.3em;
        line-height: 16px;
    }
    
    .olapic-slider-sharing .slider-olapic-icon-instagram-filled  {
        font-size: 10px;
        line-height: 17px;
    }
    .olapic-slider-header-container::after {
        display: block;
        clear: both;
        content: '';
    }
    
    .olapic .olapic-carousel-list-container {
        padding-top: 7px;
    }
    
    .olapic .olapic-slider-wrapper {
        width: 75%;
    }
    
    .olapic .olapic-carousel li {
        width: 142px;
        height: 142px;
        margin: 0 2px 0 3px;
    }
    
    .olapic .olapic-slider-footer {
        max-width: 432px;
        font-size: 11px;
    }
    
    .olapic .olapic-nav-button {
        top: 12px;
        width: 19%;
    }
}

/* - end landscape phone to portrait tablet - */

/* - Portrait Mobile - */

@media (max-width: 480px) {

	/*.olapic_items_0 .olapic-carousel,*/
 /*   .olapic_items_1 .olapic-carousel,*/
	/*.olapic_items_2 .olapic-carousel  {*/
	/*	 display: none;*/
	/*}*/

    .olapic .olapic-slider-widget {
        font-size: 16px;
    }
    
    .olapic .olapic-slider-body {
        padding-top: 0;
        width: 92%;
        background-image: none;
    }
    
	.olapic .olapic-nav-up {
		top: 0;
	}

	.olapic .olapic-nav-down {
		bottom: 0;
	}

	.olapic .olapic-slider-copy {
		margin: 8px auto auto auto;
	}

	.olapic .olapic-footer-button {
        width: 100%; 
        margin-top: 7px;
        text-align: center;
	}
    
    .olapic .olapic-footer-button a {
        width: 123px;
        padding: 8px 0;
        font-size: .7em;
        letter-spacing: .05em;
        text-transform: uppercase;
        background: #000;
        color: #fff;
    }
    
    .olapic .header-desktop-version {
        display: none;
    }
    
    .olapic .header-mobile-version {
        display: block;
        width: 83%;
    }
    
    .olapic .header-mobile-version h3 {
        line-height: 25px;
        letter-spacing: -0.04em;
        color: #000;
    }
    
    .olapic-slider-sharing {
        text-align: right;
    }
    
    .header-mobile-version .olapic-slider-sharing i {
        width: 24px;
        height: 24px;
        margin-left: 2px;
        margin-right: 3px;
        font-size: 1.1em;
        line-height: 24px;
    }
    
    .olapic-slider-sharing span {
        display: block;
        padding: 4px 0 3px;
        letter-spacing: .13em;
        color: #f9b387;
    }
    
    .header-mobile-version .olapic-slider-sharing .slider-olapic-icon-instagram-filled {
        font-size: 1em;
    }
    
    .olapic-upload a::before {
        display: none;
    }
    
    .olapic-footer-buttons {
        display: none;
    }
    
    .olapic .olapic-nav-button {
        top: -11px;
        width: 10%;
        font-size: 4em;
    }
    
    .olapic .olapic-slider-wrapper {
        width: 82%;
        max-width: 359px;
    }
    
    .olapic .olapic-carousel li {
        width: 175px;
        height: 175px;
    }

}

@media (max-width: 320px) {
    .olapic .olapic-slider-widget {
        font-size: 12px;
    }
    
    .olapic .olapic-carousel li {
        width: 245px;
        height: 245px;
    }
}
/* - end portrait mobile - */

/* end responsive styles */

</style>