{{!-- 
you can debug any object with writing {{debug}} on this template;
use {{debug}} or {{debug someValue}}
the result will be on browser's console
--}}
		<li class="{{this.source}}" style="background-image: url({{this.images.normal}});">
			<a href="#" class="olapic-item" data-url="{{this._links.self.href}}" title="{{this.caption}}">
                <span class="olapic-type-{{this.type}}"><i></i></span>
            </a>
		</li>