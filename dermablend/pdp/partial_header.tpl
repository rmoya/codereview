{{!-- 
you can debug any object with writing {{debug}} on this template;
use {{debug}} or {{debug someValue}}
the result will be on browser's console
--}}


<div class="olapic-slider-header header-mobile-version">
    <div class="olapic-slider-header-column olapic-slider-cta">
        <h3>SHARE HOW YOU CAMOUFLAGE </h3>    
	</div>
    <div class="olapic-slider-header-column olapic-slider-sharing">
        <span>#dermablendpro</span>
        <i class='slider-icon-twitter'></i>
        <i class='slider-icon-facebook'></i>
        <i class='slider-icon-pinterest'></i>
        <i class='slider-olapic-icon-instagram-filled'></i>
    </div>
        {{>link_all_photos}}
		{{>link_uploader}}
    
</div>