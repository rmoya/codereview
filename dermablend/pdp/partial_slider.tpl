{{!-- 
you can debug any object with writing {{debug}} on this template;
use {{debug}} or {{debug someValue}}
the result will be on browser's console
--}}
<div class="olapic-slider-body">
	<div class="olapic-slider-header header-desktop-version">
    <h2 class="hp-block-title">
    <span></span>
    </h2>
    <div class="olapic-slider-header-column olapic-slider-cta">
        <h3>SEE HOW OTHERS ARE WEARING <span>#dermablendpro</span></h3>    
    </div>
    <div class="olapic-slider-header-column olapic-slider-sharing">
        <i class='slider-icon-twitter'></i>
        <i class='slider-icon-facebook'></i>
        <i class='slider-icon-pinterest'></i>
        <i class='slider-olapic-icon-instagram-filled'></i>
    </div>  
</div>
	{{>arrow_prev}}
 
	<div class="olapic-slider-wrapper">
 
		<div class="olapic-carousel-list-container" data-min-items-for-slider="">
			<ul class="olapic-carousel">
			{{#each this.photoList}}
				{{>item}}
			{{/each}}
			</ul>
            <div class="olapic-carousel-partial-container">
            </div>
		</div>
 
	</div>
 
	{{>arrow_next}}
 
</div>