{{!-- 
you can debug any object with writing {{debug}} on this template;
use {{debug}} or {{debug someValue}}
the result will be on browser's console
--}}
<div class="olapic-slider-widget olapic-slider">

	{{>header}}

	{{>slider}}

	{{>footer}}

</div>