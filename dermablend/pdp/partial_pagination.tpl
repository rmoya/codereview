{{!-- 
you can debug any object with writing {{debug}} on this template;
use {{debug}} or {{debug someValue}}
the result will be on browser's console
--}}


				<div class="olapic-pagination">
					<ul>
						{{#each this.pages}}
						<li class="{{value.class}}" rel="{{value.index}}"></li>
						{{/each}}
					</ul>
				</div>