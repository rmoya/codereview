{{!-- 
you can debug any object with writing {{debug}} on this template;
use {{debug}} or {{debug someValue}}
the result will be on browser's console
--}}
<div id="olapic-viewer-dialogreport">
	<a href="#" class="close"><i class="glyphicon glyphicon-remove"></i></a>

    <form role="form" action="{{_forms.report.action.href}}" method="{{_forms.report.method}}" id="olapic-report-form">
		<div class="form-group">
			<input type="email" class="form-control" name="email" placeholder="my@email.com">
		</div>
		<div class="form-group">
			<input type="short-text" class="form-control" name="reason" placeholder="">
		</div>
		<div class="form-group">
			<input type="submit" class="form-control" name="send" placeholder="">
			<div id="reportInvalidEmail" class="reportPhotoTextHide">Invalid email address!</div>
		</div>
	</form>

	<div class="text">
		<div id="reportText">
			Are you sure you want to report this photo?</br>
			This means it is inappropriate, has violated some law or infringes someone's rights. Reporting this photo will automatically remove it from the gallery for further review, so please be sure before reporting. Please allow 20 minutes for cache to clear and photo to be removed. 
		</div>
		<div class="reportPhotoTextHide" id="reportOK">
			Photo reported</br>
			Please allow 20 minutes for cache to clear and photo to be removed. 
		</div>
		<div class="reportPhotoTextHide" id="reportFail">
			The photo is already reported</br>
		</div>
	</div>
</div>