{{!-- 
you can debug any object with writing {{debug}} on this template;
use {{debug}} or {{debug someValue}}
the result will be on browser's console
--}}	
	<div class="container">
		<div class="row" id="viewer-wrapper">
			<div class="col-md-1 cold-md-left">
				<div class="{{#if navigation.havePrev}}viewer-previous{{else}}viewer-next viewer-nav-empty{{/if}}">
					<a href="{{navigation.prev}}" id="viewer-prev"><i class="olapic-icon-arrow-left"></i></a>
				</div>
			</div>
			<div class="col-md-10 viewer-container">
				<div class="row">
					<a class="viewer-close" href="javascript:void(0)" id="closeViewer"><i class="olapic-icon-remove"></i></a>
					<div class="col-md-7 viewer-column-left">
						<a class="report-photo" href="" title="Report photo?"><i class="olapic-icon-exclamation-sign"></i></a>
						<div class="featured-photo" style="">
							<img src="{{images.normal}}" alt="" data-height="" data-width="" id="main-image">
							<span class="type-{{type}}"></span>
						</div>
					</div><!-- end viewer-column-left -->
					<div class="col-md-5 viewer-column-right">
						
						<div class="author">
							<a class="author-info blank_link" href="{{original_source}}" target="_blank">
								<img class="author-avatar default_avatar" src="{{uploader.avatar_url}}">
								<i class="icon-source olapic-icon-{{source}}"></i>
								<div class="author-names">
									<span class="author-handlename">{{uploader.name}}</span>
									<span class="author-realname blank_link" href="{{original_source}}">{{uploader.username}}</span>
								</div>
							</a>
							<p class="photo-caption">{{caption}}</p>

							{{#ifvalue source value="twitter"}}
							<div class="tw-actions">
								<a class="twitter-action tw-reply" href="https://twitter.com/intent/tweet?in_reply_to={{source_id}}" title="Reply"></a>
								<a class="twitter-action tw-retweet" href="https://twitter.com/intent/retweet?tweet_id={{source_id}}" title="Retweet"></a>
								<a class="twitter-action tw-favorite" href="https://twitter.com/intent/favorite?tweet_id={{source_id}}" title="Favorite"></a>
							</div>
							{{/ifvalue}}
						</div><!-- end author -->

						<div class="sharing">
							<ul>
								<li>
									<a class="sharing-fb" href="{{share_url}}?fb_ref=m"><i class="olapic-icon-facebook"></i></a>
								</li>
								<li>
									<a class="sharing-tw" href="{{share_url}}" title="{{caption}}"><i class="olapic-icon-twitter"></i></a>
								</li>
								<li>
									<a class="sharing-pi" href="http://pinterest.com/pin/create/button/?url={{share_url}}&media={{images.normal}}"><i class="olapic-icon-pinterest"></i></a>
								</li>
							</ul>
						</div><!-- end sharing -->
						{{#if this.relatedProducts.length}}
						<div class="products">
							<h3>Shop This Look</h3>

							<div class="row">
								<div class="col-md-1">
									<a class="products-previous" href="#">
										<i class="olapic-icon-arrow-left"></i>
									</a>
								</div>
								<div class="col-md-10">
									<div class="products-wrapper">
										<ul class="products-list">
											{{#each relatedProducts}}
											<li>
												<a class="product-list-item" href="{{this.shop_button_url}}">
													<span class="product-list-item-image" style="background-image: url({{this.base_image.images.thumbnail}});"></span>
													<span class="product-list-item-title">{{this.name}}</span>
												</a>
											</li>
											{{/each}}
										</ul>
									</div>
								</div>
								<div class="col-md-1">
									<a class="products-next" href="#">
										<i class="olapic-icon-arrow-right"></i>
									</a>
								</div>
							</div>
						</div><!-- end of products -->
						{{/if}}

						<!-- ************************************************************************
						The following attribution MUST NOT be removed as per our licensing agreement. 
						You may change the location or the styling to better match your site, but the wording and the link must remain in the template and visible to all end users. 
						************************************************************************ -->
						<div class="newcopyright-olapic newcopyright-olapic-with-icon">
							<p><a href="http://www.olapic.com" target="_blank">Powered by Olapic</a></p>
						</div>

					</div><!-- end viewer-column-right -->
					
					{{>report}}
				</div>
			</div>
			<div class="col-md-1 cold-md-right">
				<div class="{{#if navigation.haveNext}}viewer-next{{else}}viewer-next viewer-nav-empty{{/if}}">
					<a href="{{navigation.next}}" id="viewer-next"><i class="olapic-icon-arrow-right"></i></a>
				</div>
			</div>
		</div>
	</div>
